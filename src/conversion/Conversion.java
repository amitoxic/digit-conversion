/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversion;

import java.util.Scanner;

/**
 *
 * @author amitoxic
 */
public class Conversion {

        public static void main(String[] args) {

                initmsg();

        }

        public static void initmsg() {
                int opt;
                Scanner input = new Scanner(System.in);

                System.out.println("********************************************\n"
                        + "*********Number System Converter************\n"
                        + "********************************************\n"
                        + "****  Menu List::                       ****\n"
                        + "****  1:Decimal to Binary               ****\n"
                        + "****  2:Decimal to HexaDecimal          ****\n"
                        + "****  3:Binary to Decimal               ****\n"
                        + "****  4:Binary to HexaDecimal           ****\n"
                        + "****  5:HexaDecimal to Decimal          ****\n"
                        + "****  6:HexaDecimal to Binary           ****\n"
                        + "****  7:Exit System                     ****\n"
                        + "********************************************\n"
                        + "Select Your Option(1-7):");

                opt = input.nextInt();
                process(opt);
        }

        public static void process(int x) {
                switch (x) {
                        case 1:
                                Dec2BinIP();
                                break;
                        case 2:
                                Dec2HexIP();
                                break;
                        case 3:
                                Bin2DecIP();
                                break;
                        case 4:
                                Bin2HexIP();
                                break;
                        case 5:
                                Hex2DecIP();
                                break;
                        case 6:
                                Hex2BinIP();
                                break;
                        case 7:
                                System.out.println("Exiting");
                                System.exit(0);
                                break;
                        default:

                                System.out.println("Enter Valid Option(1-7)");
                                break;

                }
        }

        public static void Dec2BinIP() {
                int dec;
                System.out.println("Enter Decimal value");
                Scanner s = new Scanner(System.in);
                dec = s.nextInt();
                //Dec2Bin(dec);
                System.out.println("Equivalent Binary:"+Dec2Bin(dec));
                result();
        }

        public static String Dec2Bin(int dec) {
                String bin = "";
                //System.out.println(Integer.toBinaryString(dec));
                while (dec > 0) {
                        int d = dec % 2;
                        bin = d + "" + bin;
                        dec = dec / 2;
                }
                return bin;
        }

        public static void Dec2HexIP() {
                int dec;
                System.out.println("conversion.conv.Dec2Hex()");
                System.out.println("Enter Decimal value");
                Scanner s = new Scanner(System.in);
                dec = s.nextInt();
                System.out.println("Equivalent HexaDecimal:"+Dec2Hex(dec));
                result();
        }

        public static String Dec2Hex(int dec) {
                int d;
                String hex = "";
                // System.out.println(Integer.toHexString(n));
                while (dec > 0) {
                        d = dec % 16;
                        char hexc[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
                        hex = hexc[d] + "" + hex;
                        dec = dec / 16;
                }
                //System.out.println(hex);
                return hex;

        }

        public static void Bin2DecIP() {
                int bin;
                System.out.println("conversion.conv.Dec2Bin()");
                System.out.println("Enter Binary value:");
                Scanner s = new Scanner(System.in);
                bin = s.nextInt();
                System.out.println(Bin2Dec(bin));
                result();
        }

        public static int Bin2Dec(int bin) {
                int dec = 0, p = 0;
                while (bin != 0) {
                        dec += ((bin % 10) * Math.pow(2, p));
                        bin = bin / 10;
                        p++;
                }
                System.out.println(dec);
                return dec;
        }

        public static void Bin2HexIP() {
                int bin;
                System.out.println("conversion.conv.Dec2Bin()");
                System.out.println("Enter Binary value:");
                Scanner s = new Scanner(System.in);
                bin = s.nextInt();
                System.out.println("Equivalent HexaDecimal:"+Bin2Hex(bin));
                result();
        }

        public static String Bin2Hex(int bin) {
                int intnum, rem;
                String hex;
                intnum = Bin2Dec(bin);
                hex = Dec2Hex(intnum);
                return hex;

        }

        public static void Hex2DecIP() {
                String hex;
                System.out.println("conversion.conv.Dec2Bin()");
                System.out.println("Enter Hex value:");
                Scanner s = new Scanner(System.in);
                hex = s.nextLine();
                String HEX = hex.toUpperCase();
                System.out.println(Hex2Dec(HEX));
                result();
        }

        public static int Hex2Dec(String n) {
                int intnum, rem;
                String hex;
                String digits = "0123456789ABCDEF";

                int dec = 0;
                for (int i = 0; i < n.length(); i++) {
                        char c = n.charAt(i);
                        int d = digits.indexOf(c);
                        dec = 16 * dec + d;
                }
                //System.out.println();
                

                return dec;
        }

        public static void Hex2BinIP() {
                String n;
                System.out.println("conversion.conv.Dec2Bin()");
                System.out.println("Enter Hex value:");
                Scanner s = new Scanner(System.in);
                n = s.nextLine();
                n = n.toUpperCase();
                System.out.println("Equivalent Binary:"+Hex2Bin(n));
                result();
        }

        public static String Hex2Bin(String n) {
                int intnum;
                String bin;
                intnum = Hex2Dec(n);
                bin = Dec2Bin(intnum);
                return bin;
        }

        public static void result() {
                char anss;

                System.out.println("Convert Again 'Y' or 'N'");
                Scanner res = new Scanner(System.in);

                anss = res.next().charAt(0);

                if (anss == 'Y' || anss == 'y') {
                        initmsg();
                } else {
                        System.exit(0);
                }
        }
}
